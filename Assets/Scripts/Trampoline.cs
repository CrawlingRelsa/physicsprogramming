﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour
{
    public float springForce = 10;

    Rigidbody rb;
    SpringJoint spring;
    float minDistance;
    Vector3 localVelocity;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        spring = GetComponent<SpringJoint>();
        minDistance = spring.maxDistance;

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            //if ((spring.minDistance - springForce * Time.fixedDeltaTime) >= 1) spring.minDistance -= springForce * Time.fixedDeltaTime;
            rb.AddForce(-transform.up * springForce * Time.fixedDeltaTime, ForceMode.Impulse);
        }
        else
        {
            //smorzo   
            rb.AddForce(transform.up * spring.spring * Time.fixedDeltaTime, ForceMode.Impulse);
        }
        //local constraint
        localVelocity = transform.InverseTransformDirection(rb.velocity);
        localVelocity.x = 0;
        localVelocity.z = 0;
        rb.velocity = transform.TransformDirection(localVelocity);
    }
}
