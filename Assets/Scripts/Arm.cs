﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arm : MonoBehaviour
{

    public enum Hand { LEFT = 1, RIGHT = -1 }
    public Hand side;
    public KeyCode key;
    public float force;

    HingeJoint constraint;
    Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        constraint = GetComponent<HingeJoint>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Input.GetKey(key))
        {
            rb.AddTorque(transform.forward * (int)side * force, ForceMode.VelocityChange);
            //rb.AddForce((transform.up + transform.right * (int)side) * force);
        }
        else
        {
            rb.AddTorque(transform.forward * -(int)side * force, ForceMode.VelocityChange);
        }
    }
}
